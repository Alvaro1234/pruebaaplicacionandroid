package com.pruebaaplicacionanddroid;

// clase que contiene las propiedades de la celda
public class ModeloCelda {        
    // propiedades que tiene cada celda
    public boolean mina; // true = hay mina / false = no hay mina
    private byte minasAlrededor; // numero de minas que hay alrededor    
    public char estadoCelda; // estado en el que se encuentra la mina
    
    // estados que puede tomar una determinada celda dentro de la propiedad de estadoCelda
    public static final char CELDACUBIERTA = '0'; // la celda se encuentra sin descubir
    public static final char CELDADESCUBIERTA = '1'; // la celda ha sido descubierta
    public static final char CELDAMARCADA = '2'; // la celda esta marcada con una bandera
    
    
    // creamos el metodo constructor para asignarle los valores por defecto a cadacelda
    ModeloCelda(){
        this.mina = false;
        this.estadoCelda = CELDACUBIERTA;        
        this.minasAlrededor = 0;
    }       

    // metodo devuelve si hay mina o no
    public boolean getMina(){
        return mina;    
    }
    
    // metodo devuelve el numero de minas que hay alredodor
    public byte getMinasAlrededor(){
        return minasAlrededor;
    }
    // metodo devuelbe el estado en el que se encuentra la celda
    public char getEstadoMina(){
        return estadoCelda;
    }
    
    // metodo para poder poner o quitr una mina 
    public void setMina(boolean mina){
        this.mina = mina;
        
    }
    
    // metodo para poner o quitar el numero de minas alrededor
    public void setMinasAlrededor(byte minasAlrededor){
        this.minasAlrededor = minasAlrededor;
                
    }
    
    // metodo para dar el estado en el que se  encuentra una celda
    public void setEstadoMina (char estadoMinas){
        this.estadoCelda = estadoMinas;
    }
}
