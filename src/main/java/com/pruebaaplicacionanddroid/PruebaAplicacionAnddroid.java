package com.pruebaaplicacionanddroid;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableRow;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class PruebaAplicacionAnddroid extends Application {

    // creamos un objeto de la clase Juego
    byte numColumnas = 6;
    byte numFilas = 5;
    byte numMines = 3;
    Juego objetoJuego = new Juego(numColumnas , numFilas, numMines);
    
    @Override
    public void start(Stage stage) {
               
        // creamos nuevos elementos
        HBox hbox = new HBox();
        Button botonDestapar = new Button("Destapar");
        TextField X = new TextField();
        TextField Y = new TextField();
        Button botonBandera = new Button("Poner Bandera");
        
        // añadimos los elementos a el panel xbox
        hbox.getChildren().addAll(botonDestapar, X, Y, botonBandera);   
        
        // alimeamos el elemento xbox
        hbox.setAlignment(Pos.CENTER);
        
        // metodo para el boton 1
        botonDestapar.setOnAction(new EventHandler<ActionEvent>() {            
            @Override
            public void handle(ActionEvent event) {
                int numeroColumnas = Integer.valueOf(X.getText());
                int numeroFilas = Integer.valueOf(Y.getText());
                objetoJuego.destaparCelda(numeroColumnas, numeroFilas);
                System.out.println(objetoJuego.toString());
                
            }
        });
        
        
        // metodo para el boton 2
        botonBandera.setOnAction(new EventHandler<ActionEvent>() {            
            @Override
            public void handle(ActionEvent event) {
                int numeroColumnas = Integer.valueOf(X.getText());
                int numeroFilas = Integer.valueOf(Y.getText());
                objetoJuego.ponerBandera(numeroColumnas, numeroFilas);
                System.out.println(objetoJuego.toString());
                
            }
        });
        

        Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
        Scene scene = new Scene(hbox, visualBounds.getWidth(), visualBounds.getHeight());

        stage.getIcons().add(new Image(PruebaAplicacionAnddroid.class.getResourceAsStream("/icon.png")));

        stage.setScene(scene);
        stage.show();
         

        System.out.println(objetoJuego.toString());
       
        
    }

    
    
    
}
