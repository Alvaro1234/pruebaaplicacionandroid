package com.pruebaaplicacionanddroid;

import java.util.Random;
import java.util.logging.Logger;

/* clase que se encarga de gestionar toda la informacion interna 
del juego sin tener que mostrar nada en pantalla*/
public class Juego {    
    // creamos el array de las casillas que va a ocupar el vector    
    ModeloCelda[][] tablero;
    // numero de celdas que contiene el juego
    private final byte NUM_COLUMNAS; // NUM_COLUMNAS cuadrante juego
    private final byte NUM_FILAS; // NUM_FILAS cuadrante juego     
    private final byte NUMERO_MINAS; // numero de ninas que contiene el juego
    
    // constante de logger
    private static final Logger LOGGER = Logger.getLogger(Juego.class.getName());
    
    // creamos el metodo constructor para generar el juego
    Juego(byte numeroColumnas, byte numeroFilas, byte numMinas){
        NUM_COLUMNAS = numeroColumnas; // NUM_COLUMNAS matriz
        NUM_FILAS = numeroFilas; // NUM_FILAS matriz
        NUMERO_MINAS = numMinas; // numero de minas 
        tablero = new ModeloCelda [numeroColumnas][numeroFilas];
        Random minaAleatoria = new Random();       
        
        ModeloCelda modeloCelda = new ModeloCelda();
        
        
        // Creamos el bucle para generar la matriz
        for (byte f=0; f < numeroFilas; f++){   // bucle filas
            for (byte c=0; c < numeroColumnas; c++){    // bucle columnas         
                ModeloCelda objetoModeloCelda = new ModeloCelda();
                tablero[c][f] = objetoModeloCelda;                
            }            
        }
        
        // Ajustar el numero de minas al tamaño del Array
        if (numMinas >= (numeroColumnas * numeroFilas)){
            LOGGER.warning("El número de las minas es superior al tamaño de las celdas");
            numMinas = (byte) (numeroColumnas * numeroFilas);
        }
        
        // variables para guardar las posiciones aleatorias donde se coloca una mina
        int columnaAleatoria; 
        int filaAleatoria; 
        byte numeroMinasColocadas = 0;
        
        // bucle para colocar en posiciones aleatorias las minas
        do {
            columnaAleatoria = minaAleatoria.nextInt(numeroColumnas);
            filaAleatoria = minaAleatoria.nextInt(numeroFilas);
            if (tablero[columnaAleatoria][filaAleatoria].getMina()){    
                LOGGER.finest("Hay una mina en la posicion:" + columnaAleatoria + "," + filaAleatoria);
            } else {
                tablero[columnaAleatoria][filaAleatoria].setMina(true);
                LOGGER.finest("Se ha generado una mina en la posición:" + columnaAleatoria + "," + filaAleatoria);
                numeroMinasColocadas ++;
            }            
        } while (numeroMinasColocadas < numMinas);
        
        // recorrer toda la matriz para hacer un recuento de minas        
        for (int f=0; f < numeroFilas; f++){
            for (int c=0; c < numeroColumnas; c++){
                if (tablero[c][f].getMina()){
                    tablero[c][f].setMinasAlrededor((byte) 9);
                } else{
                    byte minasAlrededor = 0;                
                    for (int i=(f-1); i <= (f+1) ; i++){
                        for (int o=(c-1); o <= (c+1); o++){
                            if(i>=0 && i<numeroFilas && o>=0 && o<numeroColumnas){
                                if (tablero[o][i].getMina()){
                                    minasAlrededor ++;
                                }
                            }
                        }
                    }                   
                        tablero[c][f].setMinasAlrededor(minasAlrededor);
                        LOGGER.finest("las minas que hay alrededor de la posicion  " + "posicion columas:  " + c + "posicion filas:  " + f + "son: " + minasAlrededor);
                    }
            }    
        }
    }
    
    
    
    // Metodo ToString para mostrar en pantalla el contenido de la matriz
    @Override
    public String toString(){  
        // variable que recoge el estado de la totalidad de todas las celdas
        String celda = "";       
        for (byte f=0; f < NUM_FILAS; f++){   
            for (byte c=0; c < NUM_COLUMNAS; c++){            
                if (tablero[c][f].getMina()){
                    celda += "*";
                } else {
                    celda += "#";
                    }               
            }
            celda += "\n";
        }
        
        
        // implementamos el metodo para el recuento de las minas
        for (byte f=0; f < NUM_FILAS; f++){
            for (byte c=0; c < NUM_COLUMNAS; c++){                 
                celda += tablero[c][f].getMinasAlrededor();                              
            }
            celda += "\n";
        }
        
        // mostrar el estao de la celda
        for (byte f=0; f < NUM_FILAS; f++){
            for (byte c=0; c < NUM_COLUMNAS; c++){
                switch (tablero[c][f].estadoCelda){
                    case ModeloCelda.CELDADESCUBIERTA:
                        celda += "D";
                        break;
                    case ModeloCelda.CELDACUBIERTA:
                        celda += "T";
                        break;
                    case ModeloCelda.CELDAMARCADA:
                        celda += "M";
                        break;
                }                
            }
            celda += "\n";
        }        
        return celda;
    }
    
    // creamos un metodo para destapar las celdas
    public boolean destaparCelda (int columna, int fila){
        tablero[columna][fila].setEstadoMina(ModeloCelda.CELDADESCUBIERTA);
        if (tablero[columna][fila].getMina()){
            LOGGER.finest("Se ha echo click en una mina");
            System.out.println("Fin del juego, has pinchado en una mina");
            return true;
        } else {
            if (tablero[columna][fila].getMinasAlrededor() == 0){
                LOGGER.finest("se ha echo click en una casilla que no tenia minas alrededor");
                for (int col=(columna-1); col <= (columna+1); col++){
                    for (int fil=(fila-1); fil <= (fila+1); fil++){
                        if (tablero[col][fil].getEstadoMina() == ModeloCelda.CELDACUBIERTA){
                            if (fil>=0 && fil<fila && col>=0 && col<columna){
                               this.destaparCelda(col, fil);
                               LOGGER.finest("se han descubierto las celdas que no tenian minas alrededor");
                            }
                        }
                    }
                }                
            }
            return false;
        }        
    }
    
    // creamos el metodo para marcar la celda con una bandera
    public boolean ponerBandera(int columna, int fila){
        // si la celda esta cubierta
        if (tablero[columna][fila].getEstadoMina() == ModeloCelda.CELDACUBIERTA){
            tablero[columna][fila].setEstadoMina(ModeloCelda.CELDAMARCADA);
            LOGGER.finest("Una celda que estaba cubierta le hemos puesto una bandera");
            return true;
        }
        // si la celda esta descubierta
        if (tablero[columna][fila].getEstadoMina() == ModeloCelda.CELDADESCUBIERTA){
            LOGGER.finest("La celda seleccionada estaba descubierta y no se realiza ninguna accion");
            return false;
        }
        // si la celda esta marcada
        if (tablero[columna][fila].getEstadoMina() == ModeloCelda.CELDAMARCADA){
            tablero[columna][fila].setEstadoMina(ModeloCelda.CELDACUBIERTA);
            LOGGER.finest("La celda seleccionada tenia una bandera, le hemos quitado la bandera y pasa a estar cubierta");
            return false;
        }
 
        return true;
    }
    
    // metodo para contar el numero total de banderas
    public int contarBanderas(){
        int numeroBanderas = 0;
        for (byte f=0; f < NUM_FILAS; f++){   
            for (byte c=0; c < NUM_COLUMNAS; c++){ 
                if (tablero[c][f].getEstadoMina() == ModeloCelda.CELDAMARCADA){
                    numeroBanderas ++;
                    LOGGER.finest("Se ha colocado una bandera en la posicion: " + f + " " + c );
                }                                
            }                
        }  
        return numeroBanderas;
    }
    
    // creamos un metodo para contar el numero total de las celdas destapadas
    public int numeroCeldasDestapadas(){
        int numeroCeldasDespatadas = 0;
        for (byte f=0; f < NUM_FILAS; f++){   
            for (byte c=0; c < NUM_COLUMNAS; c++){ 
                if (tablero[c][f].getEstadoMina() == ModeloCelda.CELDADESCUBIERTA){
                    numeroCeldasDespatadas ++;
                    LOGGER.finest("Se ha incrementado el numero e celdas decubiertas");
                }
            }                
        }
        return numeroCeldasDespatadas;
    }
    
    // metodo para detectar el fin de la partida
    public boolean finalPartida(){
        int numeroCeldasTotal = NUM_FILAS * NUM_COLUMNAS;
        if (this.contarBanderas() + this.numeroCeldasDestapadas() == numeroCeldasTotal){
            LOGGER.finest("Se ha terminado la partida");
            return true;            
        } else{
            return false;
            }        
    }
    
}
                

        
